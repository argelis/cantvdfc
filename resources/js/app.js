/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// importando vuetify
import Vuetify from 'vuetify'
Vue.use(Vuetify)
import 'vuetify/dist/vuetify.min.css' 
// importando vuetify

// ** Vuetify ICONS **
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'
// Fin Vuetify ICONS **//


Vue.component('example-component', require('./components/ExampleComponent.vue').default);

import App from './components/App.vue'
import router from './router/index'

const app = new Vue({
    el: '#app',
    components: { App },
    router
});
