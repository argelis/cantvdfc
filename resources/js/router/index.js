import Vue from 'vue'
import Router from 'vue-router'
// COMPONENTES
// Layout
import Layout from '../components/Layout/Layout'
// Auth
import Login from '../components/Auth/Login'

// import NoFound from '../components/shared/404'
// IMPORTAR STORE VUEX
// import store from '../store/store'
Vue.use(Router)
// RUTAS
const router = new Router({
    routes: [{
        path: '/',
        name: 'princpial',
        component: Layout
    }
    // , {
    //     path: '*',
    //     name: '404',
    //     component: NoFound
    // }
    ],
    mode: "history"
});

export default router;